TEMPLATE = app
CONFIG += console c++17
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += \
        GerenciamentoJogos.cpp \
        infojogo.cpp \
        infojogobasquetebol.cpp \
        infojogofutebol.cpp \
        main.cpp

HEADERS += \
    GerenciamentoJogos.h \
    infojogo.h \
    infojogobasquetebol.h \
    infojogofutebol.h
