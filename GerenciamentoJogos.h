#pragma once

#include <iostream>

#include "infojogofutebol.h"

class GerenciamentoJogos  {
public:
   // GerenciamentoJogos();
    //Impressão do Menu para escolher opção
    int Menu();
    //Função que permite garantir que a escolha da opção é efetuada com sucesso / devolve opção escolhida
    int OpMenu();
    //Efetua ação consoante opção escolhida
    //void SelecOpMenu(int valor);
    void StartGame();
    //validar modalidade futebol ou basquetebol
    int selectModality();
    int typeData();
    void inputDataFut();
    void readDataFut();
    void showData();

//vector equipa A
    std::vector<InfoJogoFutebol> ReadTeamAPlayers(const std::string &nameTeamA, size_t sizeTeamA);
    std::vector<InfoJogoFutebol> ReadTeamASuplent(const std::string &nameSuplentA, size_t sizeTeamA);
private:
//      std::string m_player_name;
//      std::string m_player_position;
//      size_t m_player_score;
//      size_t m_scored_time;
//      // variaveis para ler de dados
       std::string namePlayer;
       std::string posPlayer;
       int scorePlayer;
       int timePlayer;
       std::string nameTeamA;
       std::string nameTeamB;
       int scoreTeamA;
       int scoreTeamB;
};


std::vector<InfoJogoFutebol> InputTeamPlayers(const std::string &nameTeamPlayers, size_t sizeTeam);
