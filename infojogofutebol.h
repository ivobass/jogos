#pragma once
#include "infojogo.h"
#include <iostream>
#include <string>
#include <vector>

class InfoJogoFutebol : public InfoJogo {
public:
//  InfoJogoFutebol() = default;

    InfoJogoFutebol (std::string &namePlayer, std::string &posPlayer, int scorePlayer, int timePlayer
                     ,std::string nameTeamA, std::string nameTeamB, int scoreTeamA, int scoreTeamB);

  //Gett
   const std::string &getNameTeamA();
   const std::string &getNamePlayer();
   const std::string &getPosPlayer();
   size_t getScorePlayer();
   size_t getTimePlayer();

   //std::vector<InfoJogoFutebol> readTeamPlayers(const std::string &nameTeamA, size_t team_size);
   //std::vector<InfoJogoFutebol> ReadTeamSuplent(const std::string &nameSuplent, size_t team_size);
   void adicionarFut(std::unique_ptr<InfoJogoFutebol> valor);
   void adicionarBask(std::unique_ptr<InfoJogoFutebol> valor);

   //o using std::vector<std::unique_ptr<InfoJogo>>::push_back;


     std::vector<std::unique_ptr<InfoJogoFutebol>> m_jogosFutebol;

     //vai no basquetebol
//     std::vector<std::unique_ptr<InfoJogoBasquetebol>> m_jogosBasquetebol;

   //introduzir dados de diferentes modalidades
//   void inputDataFut();
 void inputDataBask();
   //ler dados via ficheiro de diferentes modalidades
//   void readDataFut();
 //  void readDataBask();
   void ordenaFut();
    void ordenaBask();
    void imprimeFut();
    void imprimeBask();
   void imprimeJogo();
   void validar();


  friend std::ostream &operator<<(std::ostream &os, const InfoJogoFutebol &other);

private:
  std::string m_nameTeamA;
  std::string m_namePlayer;
  std::string m_posPlayer;
  size_t m_scorePlayer;
  size_t m_timePlayer;

  // variaveis para ler de dados
//   std::string namePlayer;
//   std::string posPlayer;
//   int scorePlayer;
//   int timePlayer;

//   std::string nameTeamB;
//   int scoreTeamA;
//   int scoreTeamB;
};


std::ostream &operator<<(std::ostream &os, const std::vector<InfoJogoFutebol> &other);

//std::vector<InfoJogoFutebol> InputTeamPlayers(const std::string &nameTeamPlayers, size_t sizeTeam);


