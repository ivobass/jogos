#include "GerenciamentoJogos.h"
#include <fstream>


//GerenciamentoJogos::GerenciamentoJogos() noexcept {}

//leitura de jugadores titulares
  std::vector<InfoJogoFutebol> GerenciamentoJogos::ReadTeamAPlayers(const std::string &nameTeamA, size_t sizeTeamA) {
  std::vector<InfoJogoFutebol> teamA_info;
  std::string namePlayer;
  std::string posPlayer;
  size_t scorePlayer;
  size_t timePlayer;

  std::cout << "Entre o nome da equipa A " << nameTeamA << ":" << std::endl;

  for (size_t i = 1; i < sizeTeamA; i++) {
    std::cout << "Player #" << i << ":" << std::endl;
    std::cout << "    Nome do jugador : ";
    std::cin >> namePlayer;
    std::cout << "    Posição(guarda-redes, defesa, médio, avançado) : ";
    std::cin >> posPlayer;
    std::cout << std::endl;

    teamA_info.emplace_back( namePlayer,  posPlayer,  scorePlayer,  timePlayer
                            ,nameTeamA, nameTeamB,  scoreTeamA,  scoreTeamB);
  }

  return teamA_info;
}

std::vector<InfoJogoFutebol> GerenciamentoJogos::ReadTeamASuplent(const std::string &nameSuplentA, size_t sizeTeamA) {
  std::vector<InfoJogoFutebol> teamA_info_suplent;
  std::string namePlayer;
  std::string posPlayer;
  size_t scorePlayer;
  size_t timePlayer;

  std::cout << "Enter the player names of team " << nameSuplentA << ":" << std::endl;

  for (size_t i = 1; i < sizeTeamA; i++) {
    std::cout << "Player #" << i << ":" << std::endl;
    std::cout << "    Nome do jugador suplente: ";
    std::cin >> namePlayer;
    std::cout << "    Posição(guarda-redes, defesa, médio, avançado) : ";
    std::cin >> posPlayer;
    std::cout << std::endl;

    teamA_info_suplent.emplace_back( namePlayer,  posPlayer,  scorePlayer,  timePlayer
                            ,nameTeamA, nameTeamB,  scoreTeamA,  scoreTeamB);
  }

  return teamA_info_suplent;
}


//Impressão do Menu para escolher opção
int GerenciamentoJogos::Menu(){
    int opmenu;
   // system("clear");
    std::cout << "====================================================" << std::endl;
    std::cout << "*       AF2 - 21093 - Programação por Objectos     *" << std::endl;
    std::cout << "====================================================" << std::endl;
    std::cout << "*                                                  *" << std::endl;
    std::cout << "*           ...Menu Principal...                   *" << std::endl;
    std::cout << "*                                                  *" << std::endl;
    std::cout << "*        1. Introduzir modalidade e dados do jogo  *" << std::endl;
    std::cout << "*        2. Ler ficheiro com jogo Futebol          *" << std::endl;
    std::cout << "*        3. Ler ficheiro com Jogo Basquetebol      *" << std::endl;
    std::cout << "*        4. Listagem de Resultados de Futebol      *" << std::endl;
    std::cout << "*        5. Listagem de Resultados de Basquetebol  *" << std::endl;
    std::cout << "*        6. Sair                                   *" << std::endl;
    std::cout << "====================================================" << std::endl;

    do{
        std::cout << std::endl << "Selecione a sua opcão de entrada:: "; //solicitamos a opcao
        std::cin >> opmenu;   //obtemos a opcao do input

        //se a opcao nao estiver no intervalo permitido
        //avisamos o utilizador e limpamos o buffer da entrada para evitar erros
        if(opmenu<1 || opmenu>6){
            std::cout << "Introduza uma opcao valida! Entre 1 e 6." << std::endl;
            std::cin.clear();
            std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
        }

    }while(opmenu<1 || opmenu>6);

    //limpamos o buffer da entrada para evitar erros
    std::cin.clear();
    std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
    return opmenu; //devolvemos a opcao escolhilda
}

//////Função que permite garantir que a escolha da opção é efetuada com sucesso / devolve opção escolhida
//int Gerenciamento::OpMenu(){
//    int op{-1};
//    std::cout << "Entre a sua Opção:" << std::endl;
//    std::cin >> op;
//    return op;
//}


//Efetua ação consoante opção escolhida
void GerenciamentoJogos::StartGame(){
    int op = -1;
    int mod{-1};

   do{
        op = Menu();
        switch(op)
        {
        case 1: // Adicionar
            mod = selectModality();
            if (mod == 1){
                std::cout << "Introduzir Dados Fut" << std::endl;
                inputDataFut();
            } else if (mod == 2){
                std::cout << "Introduzir Dados Bask" << std::endl;

            }
            break;
        case 2: //ler ficheiro
            std::cout << "Ler ficheiro txt com dados de futebol"<< std::endl;
                readDataFut();
            break;
        case 3:
      std::cout << "opçao 3:" << std::endl;

            break;
        case 4: //listar por  (do maior para mais pequeno)
                 // showData();
      std::cout << "opçao 4:" << std::endl;
            break;

        case 5: //faz
      std::cout << "opçao 5:" << std::endl;

      std::cout << std::endl << "-------------------------------------------" << std::endl;
            break;

        case 6:
            std::cout << "opçao 6:" << std::endl;
            std::cout << std::endl << std::endl << "\t\tObrigado" << std::endl;
            break;
        default:
            std::cout << std::endl << "Opção inválida." << std::endl;
            break;
        }

    }while (op !=6);
    std::cout << std::endl << std::endl << "\t Obrigado por utilizar nosso programa" << std::endl;
}

//seleciona a modalidade
int GerenciamentoJogos::selectModality(){
   int modalidade =-1; //definimos uma opcao invalida

     //apresentamos o menu
    std::cout << "************** Escolha o tipo de Modalidade Futebol ou Basketbol **************" << std::endl;
    std::cout << "1 - Futebol" << std::endl;
    std::cout << "2 - Basketbol" << std::endl;
    std::cout << "3 - Voltar ao menu principal" << std::endl;

    //enquanto o utilizador nao quiser sair entramos num loop
    //para se sair tem que se escolher '3'
    do{
        std::cout << std::endl << "Opcao: "; //solicitamos a opcao
        std::cin >> modalidade;   //obtemos a opcao do input

        //se a opcao nao estiver no intervalo permitido
        //avisamos o utilizador e limpamos o buffer da entrada para evitar erros
        if(modalidade<1 || modalidade>3){
            std::cout << "Introduza uma opcao valida! Entre 1 e 2." << std::endl;
            std::cin.clear();
            std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
        }
    }while(modalidade<1 || modalidade>3);

    //limpamos o buffer da entrada para evitar erros
    std::cin.clear();
    std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
    return modalidade; //devolvemos a opcao escolhilda

}

//seleciona a tipo de dados
int GerenciamentoJogos::typeData(){
   int td =-1; //definimos uma opcao invalida

     //apresentamos o menu
    std::cout << "************** Escolha o tipo de entradad de Dados Ficheiro ou Teclado **************" << std::endl;
    std::cout << "1 - Ficheiro" << std::endl;
    std::cout << "2 - Teclado" << std::endl;
    std::cout << "3 - Voltar ao menu principal" << std::endl;

    //enquanto o utilizador nao quiser sair entramos num loop
    //para se sair tem que se escolher '3'
    do{
        std::cout << std::endl << "Opcao: "; //solicitamos a opcao
        std::cin >> td;   //obtemos a opcao do input

        //se a opcao nao estiver no intervalo permitido
        //avisamos o utilizador e limpamos o buffer da entrada para evitar erros
        if(td<1 || td>3){
            std::cout << "Introduza uma opcao valida! Entre 1 e 3." << std::endl;
            std::cin.clear();
            std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
        }
    }while(td<1 || td>3);

    //limpamos o buffer da entrada para evitar erros
    std::cin.clear();
    std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
    return td; //devolvemos a opcao escolhilda

}

//introduzir dados futbol
void GerenciamentoJogos::inputDataFut(){
    //variaveis para ler de dados
   std::string nameTeamA;
   int sizeTeam = 11;
   int tipo = typeData();
       if (tipo ==1 ){
           readDataFut();
       }else if (tipo == 2){
           //interação com o utilizador


           std::cout << "Entre o nome da equipa A:" << std::endl;
           std::cin >> nameTeamA;
           std::cout << "Introduzir os 11 titulares" << std::endl;

           //FALTA GRAVAR OS DADOS NO VECTOR
           auto TeamInfo = InputTeamPlayers(nameTeamA, sizeTeam);
           std::cout << "=====================" << std::endl;

           std::cout << "Dados Registrados: "
                     << std::endl
                     << TeamInfo
                     << std::endl;
       }

}

void GerenciamentoJogos::readDataFut(){

     std::string line;
     std::string nameTeamA;
     std::string namePlayer;
     std::string posPlayer;
     int scorePlayer = 0;
     int timePlayer = 0;
     std::string nameTeamB;
     int scoreTeamA = 0;
     int scoreTeamB = 0;
     int sizeTeamA {11};
     auto InfoF = InfoJogoFutebol(namePlayer,posPlayer,scorePlayer,timePlayer,
                                    nameTeamA, nameTeamB, scoreTeamA, scoreTeamB);
         std::cout << "Ler ficheiro o nome da equipa A:" << std::endl;

         std::ifstream futebolteam{"futebolteam.txt"};
         if (futebolteam.is_open()){
             auto buffer{std::cin.rdbuf(futebolteam.rdbuf())};
             std::cout << "Lendo ficheiro txt......\n";
             while(getline(futebolteam, line)){
               std::cin >> namePlayer >> posPlayer;
               std::cout << namePlayer << posPlayer << std::endl;

               InfoF.adicionarFut(std::make_unique<InfoJogoFutebol>(namePlayer,posPlayer,scorePlayer,timePlayer
                                                            , nameTeamA, nameTeamB, scoreTeamA, scoreTeamB));

               auto TeamInfo = ReadTeamAPlayers(nameTeamA, sizeTeamA);

             }
         std::cin.rdbuf(buffer);
         }
         //para gravar ficheiro
         std::ofstream myfile;
         myfile.open ("InfoJogoFutebol.txt");
         for (size_t i = 1; i < 12; i++){
         if(myfile.is_open()){
         std::cout << nameTeamA << ", " << namePlayer << ", " << posPlayer << ", " << scorePlayer << ", " << timePlayer << std::endl;
         myfile << nameTeamA << ", " << namePlayer << ", " << posPlayer << ", " << scorePlayer << ", " << timePlayer  << std::endl;
         }
         }
 }

void GerenciamentoJogos::showData(){
    //mostra os dados
        std::cout << "imprimejogo futebol"<< std::endl;

        //auto futebol = InfoJogoFutebol(namePlayer,posPlayer,scorePlayer,timePlayer
    //                                 ,nameTeamA,nameTeamB,scoreTeamA,scoreTeamB);
        //podemos fazer assim
//        InfoJogoFutebol info(namePlayer,posPlayer,scorePlayer,timePlayer
//                             ,nameTeamA,nameTeamB,scoreTeamA,scoreTeamB);
        //futebol.imprimeJogo();

        std::cout << "imprimeFut"<< std::endl;
        std::cout << std::endl;
        //futebol.imprimeFut();
        std::cout << "Imprime InfoJogo"<< std::endl;
        std::cout << std::endl;
       // auto info = InfoJogo(nameTeamA,nameTeamB,scoreTeamA,scoreTeamB);
        //info.imprime();
        //std::cout << "tamanho do vector" << futebol.m_jogosFutebol.size();
        std::cout << std::endl;
}
