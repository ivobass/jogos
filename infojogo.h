#pragma once

#include <string>

class InfoJogo
{
public:
  // construtor principal
  InfoJogo(std::string nameTeamA,std::string nameTeamB,int scoreTeamA,int scoreTeamB);

  // Getters
  std::string getTeamA() const;   //nome da equipa A
  std::string getTeamB() const;   //nome da equipa B
  int getScoreTeamA() const;      //score da equipa A
  int getScoreTeamB() const;      //score da equipa B
//  int getScoreTotal() const;      //resultado final de cada partida
//  std::string getPlayers() const; //jugadores e posições
  std::string getChanges() const; //substituções e expulsões
  int getPlayerScore() const;     //score por jugador
  int getPlayerTime() const;      //tempo em que jugador marca
  // função imprimir na tela
  void imprime() const;

  // Setters
  bool setScoreTeamA(int valor);
  bool setScoreTeamB(int valor);
private:
  std::string m_nameTeamA;
  std::string m_nameTeamB;
  int m_scoreTeamA;
  int m_scoreTeamB;
//  int m_totalScore;
//  std::string m_getPlayers;
  std::string m_changes;
  int m_playerScore;
  int m_playerTime;
};


