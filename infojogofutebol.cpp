#include "infojogofutebol.h"
#include <string>
#include <fstream>

//construtor filho
InfoJogoFutebol::InfoJogoFutebol(std::string &namePlayer, std::string &posPlayer, int scorePlayer, int timePlayer
                                  ,std::string nameTeamA, std::string nameTeamB, int scoreTeamA, int scoreTeamB)

    : InfoJogo(nameTeamA, nameTeamB, scoreTeamA, scoreTeamB)
{
    m_namePlayer = namePlayer;
    m_posPlayer = posPlayer;
    m_scorePlayer = scorePlayer;
    m_timePlayer = timePlayer;

}

//InfoJogoFutebol::InfoJogoFutebol(const std::string &namePlayer
//                                 , const std::string &posPlayer
//                                 , size_t scorePlayer
//                                 , size_t timePlayer) : m_namePlayer{namePlayer}
//                                                      , m_posPlayer{posPlayer}
//                                                      , m_scorePlayer{scorePlayer}
//                                                      , m_timePlayer{timePlayer}
//{
//    //construtor vazio
//}


//getters
const std::string &InfoJogoFutebol::getNameTeamA() {return m_nameTeamA;  }

const std::string &InfoJogoFutebol::getNamePlayer() { return m_namePlayer; }

const std::string &InfoJogoFutebol::getPosPlayer() { return m_posPlayer; }

size_t InfoJogoFutebol::getScorePlayer() { return m_scorePlayer; }

size_t InfoJogoFutebol::getTimePlayer() { return m_timePlayer; }


//para melhor visualização utlizamos esta função para formatar melhor o texto
std::ostream &operator<<(std::ostream &os, const InfoJogoFutebol &other) {
  os << "Score: " << std::endl
     << "\tNome do Jugador    : " << other.m_namePlayer << std::endl
     << "\tPosição do Jugador : " << other.m_posPlayer << std::endl
     << "\tGolos do Jugador   : " << other.m_scorePlayer << std::endl
     << "\tTempo que marcou   : " << other.m_timePlayer << std::endl
     << std::endl;
  return os;
}

std::ostream &operator<<(std::ostream &os, const std::vector<InfoJogoFutebol> &other) {
  for(const auto &current : other) os << current;
  os << std::endl;
  return os;
}



//valida jogadores titulares futebol
void InfoJogoFutebol::adicionarFut(std::unique_ptr<InfoJogoFutebol> valor) {

  m_jogosFutebol.push_back(std::move(valor));   // validar ou introduzir jogadores move indica ao compilador que ele não quer uma cópia, mas sim, mover o elemento.
}


//ordena jogadores futebol
void InfoJogoFutebol::ordenaFut(){
    std::sort(std::begin(m_jogosFutebol),std::end(m_jogosFutebol));

}
////ordena jogadores Basketebol
//void InfoJogoFutebol::ordenaBask(){
//    std::sort(std::begin(m_jogosBasquetebol),std::end(m_jogosBasquetebol));

//}
//lee os dados dos jugadores de futebol
//void InfoJogoFutebol::imprimeFut() {
//  for (auto& valoratual : m_jogosFutebol) {
//    valoratual->imprimeJogo();
//    std::cout << "tamanho do vector" << m_jogosFutebol.size();
//    std::cout << std::endl;
//  }
//  return;
//}
////lee os dados dos jugadores de basketebol
//void InfoJogoFutebol::imprimeBask() {
//  for (auto& valoratual : m_jogosBasquetebol) {
//    valoratual->imprimeJogo();
//  }
//  return;
//}











////introduzir dados futbol
//void InfoJogoFutebol::inputDataFut(){
//    //variaveis para ler de dados
//   std::string nameTeamA;
//   int sizeTeam = 11;
//   GerenciamentoJogos g;
//   int tipo = g.typeData();
//       if (tipo ==1 ){
//           g.readDataFut();
//       }else if (tipo == 2){
//           //interação com o utilizador


//           std::cout << "Entre o nome da equipa A:" << std::endl;
//           std::cin >> nameTeamA;
//           std::cout << "Introduzir os 11 titulares" << std::endl;
//           auto TeamInfo = InputTeamPlayers(nameTeamA, sizeTeam);

//       }

//}


//void InfoJogoFutebol::readDataFut(){

//     std::string line;
//     std::string nameTeamA;
//     std::string namePlayer;
//     std::string posPlayer;
//     int scorePlayer;
//     int timePlayer;
//     std::string nameTeamB;
//     int scoreTeamA;
//     int scoreTeamB;

//         std::cout << "Ler ficheiro o nome da equipa A:" << std::endl;

//         std::ifstream futebolteam{"futebolteam.txt"};
//         if (futebolteam.is_open()){
//             auto buffer{std::cin.rdbuf(futebolteam.rdbuf())};
//             std::cout << "Lendo do arquivo\n";
//             while(getline(futebolteam, line)){
//               std::cin >> namePlayer >> posPlayer;
//               std::cout << namePlayer << ", " << posPlayer << std::endl;

//               adicionarFut(std::make_unique<InfoJogoFutebol>(namePlayer,posPlayer,scorePlayer,timePlayer
//                                                            , nameTeamA, nameTeamB, scoreTeamA, scoreTeamB));

//             }
//         std::cin.rdbuf(buffer);
//         }
//         //para gravar ficheiro
//         std::ofstream myfile;
//         myfile.open ("InfoJogoFutebol.txt");
//         for (size_t i = 1; i < 12; i++){
//         if(myfile.is_open()){
//         std::cout << nameTeamA << ", " << namePlayer << ", " << posPlayer << ", " << scorePlayer << ", " << timePlayer << std::endl;
//         myfile << nameTeamA << ", " << namePlayer << ", " << posPlayer << ", " << scorePlayer << ", " << timePlayer  << std::endl;
//         }
//         }
// }

//introduzir dados Basketebol
void InfoJogoFutebol::inputDataBask(){
    std::cout << "Entre dados de basquete \t" << std::endl;
}

